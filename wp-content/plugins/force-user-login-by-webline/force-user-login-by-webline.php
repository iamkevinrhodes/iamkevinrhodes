<?php
/*
 * Plugin Name: Force User Login by Webline
 * URI: http://www.weblineindia.com
 * Description: This plugin provides a feature to make your site restricted and user is required to login to view protected pages.
 * Author: Weblineindia
 * Author URL: http://www.weblineindia.com
 * Version: 1.0.1
 * Network: false
 */


define( 'FL_VERSION', '1.0.1' );

/* Activation Hook Start */

register_activation_hook( __FILE__, 'activateForceLogin' );

function activateForceLogin() {
    update_option('fl_version', FL_VERSION);
}

/* Activation Hook End */


if(is_admin()) {
    
    
    add_action('admin_menu', 'addForceLoginMenu');
    add_action('admin_init', 'plugin_admin_init');
    
    function addForceLoginMenu() {
        add_menu_page('Force User Login', 'Force User Login', 'manage_options', 'force-user-login', 'forceUserLogin');
    }
    
    function forceUserLogin() {
       ?>
       	<div class="wrap">
         <?php settings_errors(); ?>
        <form action="options.php" method="POST">
            <?php settings_fields( 'force-user-login-options-group' ); ?>
            <?php do_settings_sections( 'force-user-login' ); ?>
            <?php submit_button(); ?>
        </form>
   		</div>
		 
       <?php 
    }  
    
    function plugin_admin_init(){
    	
    	register_setting( 'force-user-login-options-group', 'force-user-login-options');
    	add_settings_section('force-user-login-main', 'Force User Login Settings', 'force_user_login_section_text', 'force-user-login');
    	add_settings_field('enable-force-login', 'Enable Force User Login', 'enable_force_login_checknox', 'force-user-login', 'force-user-login-main');
    	add_settings_field('redirect-after-login', 'Redirect User after login', 'redirect_user_after_login_dropdown', 'force-user-login', 'force-user-login-main');
    }
    
    function force_user_login_section_text() {
    	
    }
    
    function enable_force_login_checknox() {
    	
    	$options = get_option('force-user-login-options');
    	
		echo "<input id='enable-force-login' name='force-user-login-options[enable-force-login]' size='40' type='checkbox' value='1' ".checked( 1, $options['enable-force-login'], false ) ." />";
    	
    }
    
    function redirect_user_after_login_dropdown() {
    	$options = get_option('force-user-login-options');
    	
    	?>    	
    		<select name='force-user-login-options[redirect-after-login]'>
	    	<?php 
	    	 $pages = get_posts( array( 'post_type' => array( 'page' ), 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC', 'post_status' => 'any'  ) );
	       
	        if ( is_array( $pages ) && ! empty( $pages ) ) {
	        	echo '<option value="">-- ' . __( 'Choose One') . ' --</option>';
	          foreach( $pages as $page ) {
	            $post_title = '' != $page->post_title ? $page->post_title : 'Untitled';
	            echo '<option value="' . esc_attr( $page->ID ) . '"' . selected( $options['redirect-after-login'], $page->ID, false ) . '>' . $post_title . '</option>';
	          }
	        } else {
	          echo '<option value="">' . __( 'No Pages Found' ) . '</option>';
	        }?>
	    	</select>
    	<?php     
    	 
    }
   
} else {

    if ( !function_exists( 'currentURL' ) ) {
        function currentURL() {
            $url  = @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["HTTP_HOST"] : 'https://'.$_SERVER["HTTP_HOST"];
            $url .= $_SERVER["REQUEST_URI"];
            return $url;
        }
    }
    
    if ( !function_exists( 'forceLogin' ) ) {
        function forceLogin() {
    
            if( !is_user_logged_in() && ( currentURL() != wp_login_url() && currentURL() != wp_registration_url() && currentURL() != wp_lostpassword_url() ) ) {
                wp_redirect( wp_login_url(), 302 );
                exit();
            }
        }
    }
    
    $pluginOpt = get_option('force-user-login-options');
    
    function my_login_redirect( $redirect_to, $request, $user ) {

        $pluginOpt = get_option('force-user-login-options');
        
        if(isset($pluginOpt['redirect-after-login']) && $pluginOpt['redirect-after-login'] > 0) {
            return get_permalink($pluginOpt['redirect-after-login']);
        } else {
            return home_url();    
        }
    }
    
    if(isset($pluginOpt['enable-force-login']) && $pluginOpt['enable-force-login'] == 1) {
        add_action('init', 'forceLogin');
        add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );
    }
    
}