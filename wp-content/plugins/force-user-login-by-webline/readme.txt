=== Force Login by Webline ===
Contributors: Weblineindia
Tags: force login, login restriction
Requires at least: 3.2
Tested up to: 4.0
License: GPL
Stable tag: 1.0.1

This plugin provides a feature to make your site restricted and user is required to login to view protected pages.
 

== Description ==

This plugin provides a feature to make your site restricted and user is required to login to view protected pages. It has two setting in admin, which are as below:

* Enable Force Login : This will enable force login feature for your site.
* Page to Redirect After login: You can select page from the list of added page, user will land to selected page after login. If not selected then it will redirect user to home page

== Installation ==

1. Unzip 'force-login-by-webline.zip' to the '/wp-content/plugins/' directory or add it by 'Add new' in the Plugins menu
2. Activate the plugin through the 'Plugins' menu
3. Go to 'Settings->Force User Login' in the admin menu