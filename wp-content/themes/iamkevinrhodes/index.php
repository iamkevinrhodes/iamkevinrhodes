<?php get_header(); ?>
    
    <body>

    <!-- ==============================================
    Search overlay
    =============================================== -->
    
    <div class="search-overlay">
        <btn class="close-overlay">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </btn>
        <?php get_search_form(); ?>
    </div>

    <!-- ==============================================
    Navbar
    =============================================== -->

    <div id="subpage-nav" class="navbar navbar-default navbar-fixed-top scroll" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">
                    <img class="img-full-responsive" src="<?php bloginfo( 'template_directory' ); ?>/images/kevin-rhodes.png" alt="I am Kevin Rhodes" width="130px">
                </a>        
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/">Back to Home</a></li>
            </ul>
        </div>
    </div>

    <!-- ==============================================
    Subhead
    =============================================== -->

    <section class="hero-image-small blog-all-post" data-stellar-background-ratio="0.6">
        <div class="overlay"></div>
        <div class="hero-image-small-title">
            <div class="title-divider"></div>
            <h1>Kevin's Blog</h1>
            <h5>
                Thoughts about design, and life
            </h5>         
        </div>
    </section>

    <!-- ==============================================
    Blog all posts
    =============================================== -->

    <section class="blog-all-posts">
        <div class="container">
            <div class="margin-btm-50">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                Categories <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <?php wp_list_categories('orderby=name&title_li='); ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="search">
                            <button type="button" class="btn btn-primary">
                                <i class="fa fa-search"></i> Search My Blog</a>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <div class="margin-btm-50">
                <div class="row">
                    <div class="col-md-4">
                        <a href="<?php the_permalink(); ?>" class="image-wrap">
                            <div class="hover-wrap">
                                <div class="overlay-img"></div>
                                <div class="overlay-txt">
                                    <i class="fa fa-file-text-o"></i>
                                </div>
                            </div>
                            <?php the_post_thumbnail('thumbnail', array( 'class'  => "img-full-responsive")); ?>
                        </a>
                    </div>
                    <div class="col-md-8">
                        <h1>    <?php the_title(); ?>   </h1>
                        <h5 class="text-muted">
                            <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?>
                            <span class="margin-l-20">
                                <i class="fa fa-bookmark"></i>
                                <?php post_read_time(); ?>
                            </span>
                        </h5>
                        <p>
                            <?php the_excerpt(); ?>
                        </p>
                        <a href="<?php the_permalink(); ?>" class="btn btn-primary btn-sm">Read more</a>
                    </div>
                </div>
            </div>

            <?php endwhile; ?>

        </div>
    </section>

    <!-- ==============================================
    Go back
    =============================================== -->

    <section class="sub-page-back">
        <div class="navigation">
            <div class="next-posts"><?php next_posts_link('More Posts <i class="fa fa-long-arrow-right"></i>'); ?></div>
            <div class="prev-posts"><?php previous_posts_link('<i class="fa fa-long-arrow-left"></i> Previous Posts'); ?></div>
        </div>
    </section>

            <?php else : ?>

            <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                <h1>Sorry friend, no posts match your critiera.</h1>
            </div>
        </div>
    </section>

    <!-- ==============================================
    Go back No Results
    =============================================== -->

    <section class="sub-page-back">
        <a href="/blog" class="btn btn-primary btn-lg">
            <i class="fa fa-long-arrow-left"></i>
            All Blog Posts
        </a>
    </section>

            <?php endif; ?>

<?php get_footer(); ?>