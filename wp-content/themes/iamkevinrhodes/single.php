<?php get_header();  ?>

    <!-- ==============================================
    Navbar
    =============================================== -->

    <div id="subpage-nav" class="navbar navbar-default navbar-fixed-top scroll" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">
                    <img class="img-full-responsive" src="<?php bloginfo( 'template_directory' ); ?>/images/kevin-rhodes.png" alt="I am Kevin Rhodes" width="130px">
                </a>        
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/blog">All Blog Posts</a></li>
            </ul>
        </div>
    </div>

<?php while(have_posts()) : the_post(); ?>

    <!-- ==============================================
    Subhead
    =============================================== -->
    
<?php global $post; ?>
<?php
$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );
?>
    <section class="hero-image-small blog-post" style="background-image: url(<?php echo $src[0]; ?> ) !important; ?>;)" data-stellar-background-ratio="0.6">
        <div class="overlay"></div>
        <div class="hero-image-small-title">
            <div class="title-divider"></div>
            <h1><?php the_title(); ?></h1>
            <h5>
                <img class="avatar-sm" src="<?php bloginfo( 'template_directory' ); ?>/images/kevin-rhodes.jpg" alt="I am Kevin Rhodes">
                <span class="subhead-date">
                    <?php the_author(); ?> <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?>
                </span>
                <i class="fa fa-bookmark"></i>
                <?php post_read_time(); ?>
            </h5>         
        </div>
    </section>

    <!-- ==============================================
    Blog single post
    =============================================== -->

    <section class="blog-single-post">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <?php the_content(''); ?>
                </div>
            </div>
        </div>
    </section>

    <!-- ==============================================
    After blog
    =============================================== -->

    <section class="blog-single-post-after">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <ul class="post-tags">
                        <?php the_category(); ?>
                    </ul>
                    <div class="post-share">
                        <strong>Follow Kevin:</strong>
                        <ul class="post-social">
                            <li>
                                <a href="https://www.facebook.com/profile.php?id=1800765934" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/iamkevinrhodes" target="_blank">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="http://instagram.com/iamkevinrhodes/" target="_blank">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/in/iamkevinrhodes" target="_blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="post-author">
                        <img class="avatar-md post-avatar" src="<?php bloginfo( 'template_directory' ); ?>/images/kevin-rhodes.jpg" alt="I am Kevin Rhodes">
                        <h3>Kevin Rhodes</h3>
                        <p>
                            Hi, I'm Kevin. A designer living in San Diego, California. I hope you enjoyed the post, be sure to comment below and be my friend on social media.
                        </p>
                    </div>
                    <div class="post-comments">
                        <?php comments_template(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; wp_reset_query(); ?>

    <!-- ==============================================
    Go back
    =============================================== -->

    <section class="sub-page-back">
        <a href="/blog" class="btn btn-primary btn-lg">
            <i class="fa fa-long-arrow-left"></i>
            All Blog Posts
        </a>
    </section>

<?php get_footer();  ?>