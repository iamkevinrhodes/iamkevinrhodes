$( document ).ready(function() {

	/*============================================
	Preloader
	==============================================*/

    $(document).ready(function() {
        $(window).load(function(){
        $('.preloader').fadeOut(1000,function(){ $(this).remove(); });
    	});
    });

	/*============================================
	Navbar
	==============================================*/

	$(window).scroll(function(){
		if($(window).scrollTop() <= 50) {
			$("#home-nav").removeClass("scroll");
	    } else {
	        $("#home-nav").addClass("scroll");
	    }
	});

   	/*============================================
	Closes the Responsive Menu on Menu Item Click
	==============================================*/

	$('.navbar-collapse ul li a').click(function() {
	    $('.navbar-toggle:visible').click();
	});

	/*============================================
	CSS-Tricks smooth-scrolling
	http://css-tricks.com/snippets/jquery/smooth-scrolling/
	==============================================*/

    // Scrolls to the selected menu item on the page
    $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') || location.hostname === this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top-70	// Height of fixed nav
                    }, 1000);
                    return false;
                }
            }
        });
    });

	/*============================================
	Isotope
	==============================================*/

	var $container = $('#container');
	// init
	$(window).load(function() {
		$container.isotope({
		  // options
		  layoutMode: 'fitRows'
		});
	});
	// filter items on button click
	$('#filters').on( 'click', 'button', function() {
	  var filterValue = $(this).attr('data-filter');
	  $container.isotope({ filter: filterValue });
	});

	/*============================================
	Isotope button groups toggle
	==============================================*/

	$("button.btn-isotope").click(function() {
	  // remove classes from all
	  $("button.btn-isotope").removeClass("active");
	  // add class to the one we clicked
	  $(this).addClass("active");
	});

	/*============================================
	Search overlay toggle
	==============================================*/

	$(".search").click(function() {
	  $(".search-overlay").css('display', 'block');
	});

	$(".close-overlay").click(function() {
	  $(".search-overlay").css('display', 'none');
	});

	/*============================================
	Photography active button toggle
	==============================================*/

	$('#photography-btns ul li').click(function(){
		// remove classes from all
		$('#photography-btns ul li').removeClass('active');
		// add class to the one we clicked
		$(this).addClass('active');
	});

	/*============================================
	Photography div show/hide button toggle
	==============================================*/

	$('#photography-btns ul li').click(function(){
		// if the following button is clicked
		if(jQuery(this).attr('id') === 'flickr-btn'){
			jQuery('#instagram-feed').hide();
			jQuery('#flickr-feed').fadeIn(1000);
		// or if the following button is clicked
		} else if (jQuery(this).attr('id') === 'instagram-btn') {
			jQuery('#flickr-feed').hide();
			jQuery('#instagram-feed').fadeIn(1000);
		}
	});

	/*============================================
	jFlickr feed
	==============================================*/

	/* Initalize Flickr Feed */
	$('#flickr-feed').jflickrfeed({
		limit: 12,
		qstrings: {
			id: '30881294@N06'
		}, 
		itemTemplate: '<div class="col-md-2 col-sm-3 col-xs-6"><div id="bg"><a href="{{image_b}}"target="_blank"><img src="{{image_m}}" alt="{{title}}" /></a></div></div>'
	});

	/*============================================
	Instagram feed
	==============================================*/

	/* Initalize Instagram Feed */
	jQuery.fn.spectragram.accessData={accessToken:'209153810.88665a8.9e81c6f48608418aa92cce797a9f700a',clientID:'88665a8ee17044de89518d8350b81299'};
	
	jQuery('#instagram-feed').spectragram('getUserFeed',{
	    query: 'iamkevinrhodes',
	    max: 12,
		wrapEachWith: '<div class="col-md-2 col-sm-3 col-xs-6"></div>'
	});

	/*============================================
	Flexslider for twitter feed
	==============================================*/

	$(window).load(function() {
		$('.tweet-slider').flexslider({
			slideshowSpeed: 5000,
			prevText: '<i class="fa fa-angle-left"></i>',
			nextText: '<i class="fa fa-angle-right"></i>',
			useCSS: true,
			pauseOnAction: false, 
			pauseOnHover: true,
			controlNav: false,
			directionNav: true,
			controlsContainer:".holder_arrow"
		});
	});

	/*============================================
	Flexslider for quotes
	==============================================*/

	$('.quote-slider').flexslider({
		slideshowSpeed: 5000,
		useCSS: true,
		pauseOnAction: false, 
		pauseOnHover: true,
		directionNav: false,
		animation: 'slide'
	});

	/*============================================
	Twitter fetcher
	==============================================*/

	var config1 = {
	  "id": '534991704836042752',
	  "domId": '',
	  "maxTweets": 3,
	  "enableLinks": true,
	  "showUser": false,
	  "customCallback": handleTweets
	};
	twitterFetcher.fetch(config1);

	function handleTweets(tweets){
	    var x = tweets.length;
	    var n = 0;
	    var element = document.getElementById('tweets');
	    var html = '<ul class="slides">';
	    while(n < x) {
	      html += '<li>' + tweets[n] + '</li>';
	      n++;
	    }
	    html += '</ul>';
	    element.innerHTML = html;

	    $('.twitter_reply_icon').html("<i class='fa fa-reply'></i>");
		$('.twitter_retweet_icon').html("<i class='fa fa-retweet'></i>");
		$('.twitter_fav_icon').html("<i class='fa fa-star'></i>");
	}

    /*============================================
	WOW animations
	==============================================*/

	wow = new WOW(
    {
      offset:       200,          // default
      mobile: false
    }
  	)
  	wow.init();

    /*============================================
	Stellar
	==============================================*/

    /*============================================
	Disable Stellar on Mobile
	==============================================*/

});