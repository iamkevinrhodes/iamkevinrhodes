<?
/*
Template Name: Single Design Portfolio
*/
?>

<?php get_header(); ?>

<body>

    <!-- ==============================================
    Navbar
    =============================================== -->

    <div id="subpage-nav" class="navbar navbar-default navbar-fixed-top scroll" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">
                    <img class="img-full-responsive" src="<?php bloginfo( 'template_directory' ); ?>/images/kevin-rhodes.png" alt="I am Kevin Rhodes" width="130px">
                </a>        
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/#portfolio">Back to Portfolio</a></li>
            </ul>
        </div>
    </div>

    <!-- ==============================================
    Subhead
    =============================================== -->

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <section class="hero-image-small portfolio-post" data-stellar-background-ratio="0.6">
        <div class="overlay"></div>
        <div class="hero-image-small-title">
            <div class="title-divider"></div>
            <h1><?php the_title(); ?></h1>
            <h5>
                <strong>Responsiblities:</strong>
<?php
$posttags = get_the_tags();
if ($posttags) {
  foreach($posttags as $tag) {
    echo $tag->name . ' '; 
  }
}
?>
            </h5>         
        </div>
    </section>

    <!-- ==============================================
    Portfolio area
    =============================================== -->

    <section class="portfolio-area">
        <div class="container">
            <div class="margin-btm-30">
                <div class="row">
                    <div class="col-md-8">
<?php $images = get_custom_field('design_portfolio_image_custom_field:to_image_src'); 
    foreach ($images as $img) {
        printf('<img class="img-full-responsive" src="%s"/>', $img);
    }
?>
                    </div>
                    <div class="col-md-4">
                        <h4>Project Overview</h4>
                        <p>
                            <?php the_content(); ?>
                        </p>
                        <h4>Project Specs</h4>
                        <ul class="portfolio-area-project-info">
                            <li>
                                <strong>Date:</strong> <?php print_custom_field('date_completed_custom_field'); ?>
                            </li>
                            <li>
                                <strong>Company:</strong> <?php print_custom_field('company_custom_field'); ?>
                            </li>
                            <li>
                                <strong>Skills/Tools:</strong> <?php print_custom_field('skills_tools_custom_field'); ?>
                            </li>
                            <li>
                                <strong>Website:</strong> <a href="http://<?php print_custom_field('website_custom_field'); ?>" target="_blank"><?php print_custom_field('website_custom_field'); ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; // end of the loop. ?>

    <!-- ==============================================
    Go back
    =============================================== -->

    <section class="sub-page-back">
        <a href="/#portfolio" class="btn btn-primary btn-lg">
            <i class="fa fa-long-arrow-left"></i>
            Back to Portfolio
        </a>
    </section>

<?php get_footer(); ?>