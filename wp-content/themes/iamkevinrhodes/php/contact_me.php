<?php 

  // one last check to see if fields passed are empty
  // (just in case jqBootstrapValidation js fails) 
 
 if(empty($_POST['name'])   ||    
    empty($_POST['email'])  ||
    empty($_POST['message'])||   
    !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))    
  {     
       echo "No arguments Provided!";   return false;    
  } 

  // define array from html contact form inputs

  $name = $_POST['name']; 
  $email_address = $_POST['email']; 
  $message = $_POST['message'];      
 
 // customize your mail message

 $to = 'hello@iamkevinrhodes.com';  // <============================ PUT YOUR EMAIL HERE
 $email_subject = "iamKevinRhodes Contact Form Message";  // <============== PUT YOUR SUBJECT HERE

 // actual main message

 $email_body = "Sender Name: \n$name \n\nSender Email: \n$email_address\n\nMessage: \n$message"; 
 $headers = "From: $name\n"; 
 $headers .= "Reply-To: $email_address";     
 
 mail($to,$email_subject,$email_body,$headers); return true;

?>