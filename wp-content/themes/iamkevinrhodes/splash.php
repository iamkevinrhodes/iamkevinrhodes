<?
/*
Template Name: Splash
*/
?>

<?php get_header(); ?>

    <body id="home" data-spy="scroll" data-target="#home-nav" data-offset="300">


        <!-- ==============================================
        Preloader
        =============================================== -->

        <div class="preloader">
            <div class="preloader-content">
                Just a sec...
                 <div class="signal"></div>
            </div>
        </div>

        <!-- ==============================================
        Showcases
        =============================================== -->

        <div class="csswinner">
            <a href="http://www.csswinner.com/details/iamkevinrhodescom/8509" target="_blank"></a>
        </div>

        <div class="awwwards">
            <a href="http://www.awwwards.com/best-websites/iamkevinrhodes-com-personal-portfolio-site/?subsection=open" target="_blank"></a>
        </div>

        <!-- ==============================================
        Navbar
        =============================================== -->

        <div id="home-nav" class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#home">
                        <img class="logo" src="<?php bloginfo( 'template_directory' ); ?>/images/kevin-rhodes.png" alt="I am Kevin Rhodes" width="130px">
                    </a>        
                </div>
               <div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#home">Home</a></li>
                        <li><a href="#profile">Profile</a></li>
                        <li><a href="#skills">Skills</a></li>
                        <li><a href="#portfolio">Portfolio</a></li>
                        <li><a href="#photography">Photography</a></li>
                        <li><a href="#blog">Blog</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- ==============================================
        Hero image
        =============================================== -->

        <div class="hero-image splash-page" data-stellar-background-ratio="0.6">
            <div class="hero-content above-overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h1>
                                I am Kevin Rhodes <br />
                                and I am a
                                <span class="txt-rotate" data-period="2000" data-rotate='[ "designer.", "front-end developer.", "photographer.", "photo.", "free thinker.", "blogger." ]'></span> 
                            </h1> 
                            <a class="btn btn-default btn-lg" href="#profile">
                                Get to Know Me
                            </a> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="overlay"></div>
        </div>

        <!-- ==============================================
        About
        =============================================== -->

        <section class="section-about-top" id="profile">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        <h1 class="wow fadeInUp">
                            I'm so happy you're visiting<span class="cursor-tag-dark">_</span>
                        </h1>
                        <h3 class="wow fadeInUp">
                            I designed this site so you can get to know me on a personal level. My facination with experience design, some fun works, my photography, my blog, my favorite quotes, and so much more.
                        </h3>
                    </div>
                </div>
            </div>
            <img class="avatar-lg wow bounceIn" src="<?php bloginfo( 'template_directory' ); ?>/images/kevinrhodes.jpg" >
        </section>
        <section class="section-about-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 text-center wow bounceIn">
                        <div class="personal-blurb">
                            <span class="icons">
                                <i class="fa fa-heart"></i>
                            </span>
                            <h4>
                                Why I love design
                            </h4>
                            <p class="text-muted">
                                Design allows me to speak any language. Solving problems through type, space, and image is the most universal way to communicate. It's everything and anything.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 text-center wow bounceIn" data-wow-delay="0.6s">
                        <div class="personal-blurb">
                            <span class="icons">
                                <i class="fa fa-coffee"></i>
                            </span>
                            <h4>
                                Why I love coffee
                            </h4>
                            <p class="text-muted">
                                When I sip coffee, I ponder the delicate process it took to get from a bean to my cup. From the growing, harvesting, processing, roasting, and brewing, it's truly miraculous.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 text-center wow bounceIn" data-wow-delay="0.9s">
                        <div class="personal-blurb">
                            <span class="icons">
                                <i class="fa fa-sun-o"></i>
                            </span>
                            <h4>
                                San Diego is my life
                            </h4>
                            <p class="text-muted">
                                My culture and personality have been greatly influenced by the warm currents of the Pacific Ocean. My biggest inspirations happen on San Diego's beautiful waterfront.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- ==============================================
        What I offer
        =============================================== -->

        <section class="global-section section-offer" id="skills">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center wow fadeInUp">
                            What I offer<span class="cursor-tag">_</span>
                        </h1>
                        <p class="lead text-center text-muted margin-btm-50 wow fadeInUp">
                            These are the skills I'm most proud of
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 wow fadeInUp">
                        <div class="offer margin-btm-30">
                            <span class="icons offer-icon">
                                <i class="fa fa-desktop"></i>
                            </span>
                            <h4>Interface Design</h4>
                            <p>
                                To provide a good interface, I must understand the users needs. That is why I investigate projects systematically with stakeholders and constantly iterate designs.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 wow fadeInUp">
                        <div class="offer margin-btm-30">
                            <span class="icons offer-icon">
                                <i class="fa fa-code"></i>
                            </span>
                            <h4>Front End Development</h4>
                            <p>
                                I believe in well organized CSS files, semantic HTML, and pixel perfection. This can only be achieved by a front-end developer who understands design.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 wow fadeInUp">
                        <div class="offer margin-btm-30">
                            <span class="icons offer-icon">
                                <i class="fa fa-pencil"></i>
                            </span>
                            <h4>Visual Design</h4>
                            <p>
                                I rely heavily on graphic design to support how the user is going to interact with the product.  An aesthetically appealing site could make or break usability.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 wow fadeInUp">
                        <div class="offer">
                            <span class="icons offer-icon">
                                <i class="fa fa-building"></i>
                            </span>
                            <h4>Corporate Identity</h4>
                            <p>
                                While designing, I always adhere to corporate and brand guidelines, but at the same time, evolving and pushing limits to stay on the cutting edge.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- ==============================================
        Let's get technical
        =============================================== -->

        <section class="global-section section-technical">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 wow bounceInLeft">
                        <h1>
                            Let's get technical<span class="cursor-tag">_</span>
                        </h1>
                        <p class="margin-btm-30">
                            My interest in design started as a teenager, learning to code HTML in Notepad. I sharpened these by learning the fundamentals and principles of design. Core concepts such as gestalt psychology were fascinating. Over the last two years, I've shifted focus to UX Design. Whether I'm sketching, wireframing, prototyping, the field combines my love of psychology with my passion for design.
                        </p>
                        <a class="btn btn-primary btn-lg margin-btm-30" href="<?php bloginfo( 'template_directory' ); ?>/assets/kevinrhodesresume.pdf" target="_blank">
                            <i class="fa fa-download"></i> <span class="hidden-xs">Download My</span> Resume
                        </a>
                    </div>
                    <div class="col-md-6 wow bounceInRight" data-wow-delay="0.3s">
                        <div class="skill-bar">
                            <ul>
                                <li>
                                    <div class="bar" data-value="" style="width: 90%;">
                                        <h5>HTML5/CSS3</h5>
                                    </div>
                                    <span>9 / 10</span>
                                </li>
                                <li>
                                    <div class="bar" data-value="" style="width: 90%;">
                                        <h5>Adobe Photoshop</h5>
                                    </div>
                                    <span>9 / 10</span>
                                </li>
                                <li>
                                    <div class="bar" data-value="" style="width: 80%;">
                                        <h5>Adobe Illustrator</h5>
                                    </div>
                                    <span>8 / 10</span>
                                </li>
                                <li>
                                    <div class="bar" data-value="" style="width: 70%;">
                                        <h5>Sketch</h5>
                                    </div>
                                    <span>9 / 10</span>
                                </li>
                            </ul>
                        </div>
                    </div> 
                </div>
            </div>
        </section>

        <!-- ==============================================
        Happy employers
        =============================================== -->

        <section class="section-happy" data-stellar-background-ratio="0.6">
            <div class="overlay"></div>
            <div class="above-overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="text-center wow fadeInUp">
                                Happy Colleagues<span class="cursor-tag">_</span>
                            </h1>
                            <p class="lead text-center margin-btm-50 wow fadeInUp">
                                Some companies I've been involved with
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center">
                                <ul class="text-center">
                                    <li class="wow fadeIn">
                                        <a href="http://www.illumina.com" target="_blank" title="">
                                            <img class="img-full-responsive" src="<?php bloginfo( 'template_directory' ); ?>/images/illumina.png">
                                        </a>
                                    </li>
                                    <li class="wow fadeIn" data-wow-delay="0.3s">
                                        <a href="http://www.ashcompanies.com" target="_blank" title="">
                                            <img class="img-full-responsive" src="<?php bloginfo( 'template_directory' ); ?>/images/americanspecialtyhealth.png">
                                        </a>
                                    </li> 
                                    <li class="wow fadeIn" data-wow-delay="0.6s">
                                        <a href="http://www.active.com" target="_blank" title="">
                                            <img class="img-full-responsive" src="<?php bloginfo( 'template_directory' ); ?>/images/activenetwork.png">
                                        </a>
                                    </li> 
                                    <li class="wow fadeIn" data-wow-delay="0.9s">
                                        <a href="http://www.lnrdesigns.com" target="_blank" title="">
                                            <img class="img-full-responsive" src="<?php bloginfo( 'template_directory' ); ?>/images/lnrdesigns.png">
                                        </a>
                                    </li>
                                    <li class="wow fadeIn" data-wow-delay="1.2s">
                                        <a href="http://www.wellsfargo.com" target="_blank" title="">
                                            <img class="img-full-responsive" src="<?php bloginfo( 'template_directory' ); ?>/images/wellsfargo.png">
                                        </a>
                                    </li> 
                                </ul>   
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </section>

        <!-- ==============================================
        My works
        =============================================== -->

        <section class="global-section section-my-works" id="portfolio">
            <div class="container">
                <div class="row">
                     <div class="col-md-12">
                        <h1 class="text-center wow fadeInUp">
                            My works<span class="cursor-tag">_</span>
                        </h1>
                        <p class="lead text-center text-muted margin-btm-30 wow fadeInUp">
                            What I've done the last few years
                        </p>
                    </div>               
                </div>
                <div class="row">
                    <div class="col-md-12">
                         <!-- Isotope nav -->
                         <div class="isotope-button-container margin-btm-30">
                            <div id="filters" class="button-group">
                              <button class="btn btn-primary btn-isotope active" data-filter="*">All</button>
                              <button class="btn btn-primary btn-isotope" data-filter=".UI-UX">UI/UX</button>
                              <button class="btn btn-primary btn-isotope" data-filter=".Code">Code</button>
                              <button class="btn btn-primary btn-isotope" data-filter=".Design">Design</button>
                            </div>
                         </div>
                        <!-- /Isotope nav -->
                    </div>
                </div>
            </div>
            <!-- Isotope content -->
            <div id="container">
<?php 
    $args = array( 
        'post_type' => 'design-portfolio',
        'posts_per_page'   => 12
    );
    $the_query = new WP_Query( $args );
?>
<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="col-md-4 col-sm-6 col-xs-12
<?php
$posttags = get_the_tags();
if ($posttags) {
  foreach($posttags as $tag) {
    echo $tag->name . ' '; 
  }
}
?>
                ">
                    <a href="<?php the_permalink(); ?>" class="image-wrap">
                        <div class="hover-wrap">
                            <div class="overlay-img"></div>
                            <div class="overlay-txt">
                                <span class="portfolio-overlay-title"><?php the_title() ;?></span>
                                <span class="portfolio-overlay-category">
<?php
$posttags = get_the_tags();
if ($posttags) {
  foreach($posttags as $tag) {
    echo $tag->name . ' '; 
  }
}
?>
                                </span>
                            </div>
                        </div>
                        <?php the_post_thumbnail('thumbnail', array( 'class'  => "img-full-responsive")); ?>
                    </a>
                </div>
<?php endwhile; else: ?>
    <p>Sorry, there are no posts to display</p>
<?php endif; ?>
            <!-- /Isotope content -->
        </section>

        <!-- ==============================================
        Photography
        =============================================== -->

        <section class="global-section section-photography" id="photography">
            <div class="container">
                <div class="row">
                     <div class="col-md-12">
                        <h1 class="text-center wow fadeInUp">
                            I enjoy snapping pics<span class="cursor-tag">_</span>
                        </h1>
                        <p class="lead text-center text-muted margin-btm-30 wow fadeInUp">
                            Just a hobby of mine. Catch me on <a href="https://www.flickr.com/photos/" target="_blank">Flickr</a> and <a href="https://www.instagram.com/" target="_blank">Instagram</a>.
                        </p>
                    </div>   
                </div>
                <div class="row">
                    <div id="photography-btns" class="photography-controls">
                        <ul>
                            <li id="flickr-btn" class="active">
                                <a>
                                    <i class="fa fa-flickr"></i>
                                </a>
                            </li>
                            <li id="instagram-btn">
                                <a>
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                        </ul>          
                    </div>
                </div>
            </div>
            <div id="flickr-feed" class="photography-global-feed"></div>
            <div id="instagram-feed" class="instagram-global-feed" style="display: none;"></div>
        </section>

        <!-- ==============================================
        Tweet
        =============================================== -->

        <section class="global-section tweet-section">
            <div class="container wow flipInX">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tweet-area">
                            <div class="tweet-icon">
                                <i class="fa fa-twitter"></i>
                            </div>
                            <h3>
                                <a href="http://www.twitter.com/iamkevinrhodes" target="_blank">
                                    @iamkevinrhodes
                                </a>                    
                            </h3>
                            <div id="tweets" class="tweet-slider"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="holder_arrow"></div>
                    </div>
                </div>
            </div>
        </section>

        <!-- ==============================================
        Blog
        =============================================== -->

        <section class="global-section blog-section" id="blog">
            <div class="container">
                <div class="row">
                   <div class="col-md-12">
                        <h1 class="text-center wow fadeInUp">
                            Sometimes I blog<span class="cursor-tag">_</span>
                        </h1>
                        <p class="lead text-center text-muted margin-btm-50 wow fadeInUp">
                            Thoughts about design and living
                        </p>
                    </div>
                </div>

                    <div class="row">
<?php 
$args = array('post-type' => 'post', 'showposts' => '3');
$query = new WP_Query($args);
while($query -> have_posts()) : $query -> the_post();
?>
                    

                        <div class="col-md-4 margin-btm-30 wow bounceIn">
                            <a href="<?php the_permalink(); ?>" class="image-wrap">
                                <div class="hover-wrap">
                                    <div class="overlay-img"></div>
                                    <div class="overlay-txt">
                                        <i class="fa fa-file-text-o"></i>
                                    </div>
                                </div>
                                <?php the_post_thumbnail('thumbnail', array( 'class'  => "img-full-responsive")); ?>
                            </a>
                            <h3>
                                <?php the_title(); ?>       
                            </h3>
                            <h5 class="text-muted">
                                <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?>
                                <span class="margin-l-20">
                                    <i class="fa fa-bookmark"></i>
                                    <?php post_read_time(); ?>
                                </span>
                            </h5>
                            <p>
                                <?php the_excerpt(__('(more…)')); ?>
                            </p>
                            <a href="<?php the_permalink(); ?>" class="btn btn-primary btn-sm">Read more</a>
                        </div>

<?php endwhile; ?>


                        
                    </div>   

                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="/blog" class="btn btn-primary btn-block">All blog posts</a>
                    </div>
                </div>
            </div>
        </section>

        <!-- ==============================================
        Favorite Quotes
        =============================================== -->
        
        <section class="section-quote" data-stellar-background-ratio="0.6">
            <div class="overlay"></div>
            <div class="above-overlay">
                <div class="container">
                    <div class="row">
                         <div class="col-md-12">
                            <h1 class="text-center margin-btm-30">
                                Favorite Quotes<span class="cursor-tag">_</span>
                            </h1>
                        </div>   
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center quote-icon">
                                <i class="fa fa-quote-left"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="quote-slider">
                                <ul class="slides">
                                    <li>
                                        <div class="quote-slider-slide-container">
                                            <img class="avatar-md quote-slider-img" src="<?php bloginfo( 'template_directory' ); ?>/images/steve-jobs-profile.jpg" alt="">
                                            <p class="quote-slider-single">
                                                You can't connect the dots looking forward. You can only connect them looking backwards. So you have to trust that the dots will somehow connect in your future.
                                            </p>
                                            <p class="quote-slider-name">
                                                - Steve Jobs / Apple Inc.
                                            </p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="quote-slider-slide-container">
                                            <img class="avatar-md quote-slider-img" src="<?php bloginfo( 'template_directory' ); ?>/images/albert-einstein-profile.jpg" alt="">
                                            <p class="quote-slider-single">
                                                We can't solve problems by using the same kind of thinking we used when we created them.
                                            </p>
                                            <p class="quote-slider-name">
                                                - Albert Einstein / Theoretical Physicist
                                            </p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="quote-slider-slide-container">
                                            <img class="avatar-md quote-slider-img" src="<?php bloginfo( 'template_directory' ); ?>/images/jony-ive-profile.jpg" alt="">
                                            <p class="quote-slider-single">
                                                While ideas ultimately can be so powerful, they begin as fragile, barely formed thoughts, so easily missed, so easily compromised, so easily just squished.
                                            </p>
                                            <p class="quote-slider-name">
                                                - Jonathan Ive / Apple Inc.
                                            </p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="quote-slider-slide-container">
                                            <img class="avatar-md quote-slider-img" src="<?php bloginfo( 'template_directory' ); ?>/images/calvin-klein-profile.jpg" alt="">
                                            <p class="quote-slider-single">
                                                You must have people believe in you. You have to be a leader. Even if you are unsure and insecure, don't show it. Go with strength, conviction, and confidence.
                                            </p>
                                            <p class="quote-slider-name">
                                                - Calvin Klein / Calvin Klein Inc.
                                            </p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="quote-slider-slide-container">
                                            <img class="avatar-md quote-slider-img" src="<?php bloginfo( 'template_directory' ); ?>/images/abraham-lincoln-profile.jpg" alt="">
                                            <p class="quote-slider-single">
                                                You can please some of the people all of the time, you can please all of the people some of the time, but you can’t please all of the people all of the time.
                                            </p>
                                            <p class="quote-slider-name">
                                                - Abraham Lincoln / 16th President U.S.A.
                                            </p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="quote-slider-slide-container">
                                            <img class="avatar-md quote-slider-img" src="<?php bloginfo( 'template_directory' ); ?>/images/osho-profile.jpg" alt="">
                                            <p class="quote-slider-single">
                                                To be creative means to be in love with life. You can be creative only if you love life enough that you want to enhance its beauty, you want to bring a little more music to it, a little more poetry to it, a little more dance to it.
                                            </p>
                                            <p class="quote-slider-name">
                                                - Osho / Spiritual Teacher
                                            </p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php get_footer(); ?>