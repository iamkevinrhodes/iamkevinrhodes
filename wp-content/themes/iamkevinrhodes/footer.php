	<!-- ==============================================
    Contact Form
    =============================================== -->

    <section class="global-section contact-section" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center">
                        Say Hello<span class="cursor-tag">_</span>
                    </h1>
                    <p class="lead text-center text-muted margin-btm-50">
                        If you have a question, I'll answer
                    </p>
                </div>
            </div>
            <!-- Form -->

            
            <form name="sentMessage" id="contactForm" novalidate class="wow fadeInUp" data-wow-delay="0.4s">

            <div class="row">
                <div class="col-md-5">
                    <div class="control-group">
                        <div class="controls">
                            <input type="text" class="form-control input-lg" placeholder="Your Name" id="name" required data-validation-required-message="Please enter your name" />
                            <p class="help-block"></p>
                        </div>
                    </div> 
                    <div class="control-group">
                        <div class="controls">
                            <input type="email" class="form-control input-lg" placeholder="Your Email" id="email" required data-validation-required-message="Please enter your email" />
                            <p class="help-block"></p>
                        </div>
                    </div> 
                    <div class="control-group">
                        <div class="controls">
                            <input type="text" class="form-control input-lg" placeholder="3+1=" id="captcha" required pattern="4" data-validation-required-message="Please prove you're not a robot" />
                            <p class="help-block"></p>
                        </div>
                    </div> 
                </div>
                <div class="col-md-7">
                    <div class="control-group">
                      <div class="controls">
                       <textarea rows="7" cols="100" class="form-control input-lg" placeholder="Your Message" id="message" required data-validation-required-message="Please enter your message" minlength="5" data-validation-minlength-message="Min 5 characters" maxlength="999" style="resize:none"></textarea>
                     </div>
                   </div>   
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="success"> </div> <!-- For success/fail messages -->
                    <button type="submit" class="btn btn-primary btn-lg pull-right">
                       <i class="fa fa-paper-plane"></i> Send
                    </button>
                </div>
            </div>

            </form>
            <!-- /Form -->
        </div>
    </section>

    <!-- ==============================================
    Google maps
    =============================================== -->

    <section class="maps" id="san-diego-baby"></section>

    <!-- ==============================================
    Footer
    =============================================== -->
<!--
     <section class="social-links">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <ul class="text-center">
                            <li>
                                <a href="https://www.facebook.com/profile.php?id=1800765934" target="_blank" title="">
                                    <span>
                                        <i class="fa fa-facebook"></i> 
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/iamkevinrhodes" target="_blank" title="">
                                    <span>
                                        <i class="fa fa-twitter"></i> 
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.linkedin.com/in/iamkevinrhodes" target="_blank" title="">
                                    <span>
                                        <i class="fa fa-linkedin"></i> 
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.flickr.com/photos/iamkevinrhodes/" target="_blank" title="">
                                    <span>
                                        <i class="fa fa-flickr"></i> 
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="https://github.com/iamkevinrhodes" target="_blank" title="">
                                    <span>
                                        <i class="fa fa-github"></i> 
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="mailto:hello@iamkevinrhodes.com?subject=I have a comment for you..." title="">
                                    <span>
                                        <i class="fa fa-envelope"></i> 
                                    </span>
                                </a>
                            </li>
                        </ul>   
                    </div>
                </div>
            <div>
        </div>
    </section>
-->
    
    <section class="copyright">
        <div class="container">
            <div class="col-md-12">
                <div class="text-center">
                    &copy;<script>document.write(new Date().getFullYear())</script> - Hand coded and designed with <i class="fa fa-heart"></i> by Kevin Rhodes    
                </div>
            </div>
        </div>
    </section> 

    <!-- ==============================================
    My Custom Scripts
    =============================================== -->

    <script src="/wp-content/themes/iamkevinrhodes/js/min/iamkevinrhodes-min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="/wp-content/themes/iamkevinrhodes/js/lib/gmaps.js"></script>
    <script src="/wp-content/themes/iamkevinrhodes/js/lib/jqBootstrapValidation.js"></script>
    <script src="/wp-content/themes/iamkevinrhodes/js/lib/contact_me.js"></script>
    <script type="text/javascript">
        var map = new GMaps({
            div: '#san-diego-baby',
            lat: 32.714690,
            lng: -117.161986,
            zoom: 12,
            zoomControl: true,
            zoomControlOpt: {
            style : 'SMALL',
            position: 'TOP_LEFT'
            },
            scrollwheel: false,
            mapTypeControl: false,
            streetViewControl: false,
            panControl: false,
            // http://snazzymaps.com/style/31/red-hues (modified)
            styles:
        [{
                    stylers: [{
                        hue: "#0fb5b3"
                    }, {
                        saturation: -60
                    }]
                }, {
                    featureType: "road",
                    elementType: "geometry",
                    stylers: [{
                        lightness: 100
                    }, {
                        visibility: "simplified"
                    }]
                }, {
                    featureType: "road",
                    elementType: "labels",
                    stylers: [{
                        visibility: "off"
                    }]
                }]
        });

        map.addMarker({
            lat: 32.720524,
            lng: -117.161986,
            title: 'Address',
            icon: "/wp-content/themes/iamkevinrhodes/images/marker.png",   
            infoWindow: {
                content: '<h5 class="title">Born and Raised :)</h5><p><span class="region">And proud of it. If ever you visit here, I will give you a tour, just tweet me.</span></p>'
            }
        });
    </script>

    <!-- ==============================================
    Wordpress Footer Stuff
    =============================================== -->

    <?php wp_footer(); ?>

    </body>

</html>